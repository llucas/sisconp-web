package br.leonardo.sisconp.web.filtro;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.leonardo.sisconp.log.Log;
import br.leonardo.sisconp.web.controller.AppController;

@WebFilter(urlPatterns = { "/web/private/*" }, dispatcherTypes={DispatcherType.ERROR,DispatcherType.FORWARD, DispatcherType.REQUEST})
public class Filter  implements javax.servlet.Filter  {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	
		AppController app = (AppController) ((HttpServletRequest) request).getSession()
				.getAttribute("appController");
	
		try {
			if (app != null 
					&& app.getUsuario() != null 
					&& app.getUsuario().getId() != null
					&& app.getUsuario().getStatus() != null 
					&& app.getUsuario().getStatus()) {
				
				
				
				chain.doFilter(request, response);
			} else {
			
				
				String contextPath = ((HttpServletRequest) request).getContextPath();
				 ((HttpServletResponse)response).sendRedirect(contextPath+"/login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			String contextPath = ((HttpServletRequest) request).getContextPath();
			 ((HttpServletResponse)response).sendRedirect(contextPath+"/login");

			
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	


	
}
