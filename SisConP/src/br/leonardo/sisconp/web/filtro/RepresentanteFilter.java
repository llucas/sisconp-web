package br.leonardo.sisconp.web.filtro;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Representante;
import br.leonardo.sisconp.web.controller.AppController;

@WebFilter(urlPatterns = {"/web/private/usuario/*" }, dispatcherTypes={DispatcherType.ERROR,DispatcherType.FORWARD, DispatcherType.REQUEST})
public class RepresentanteFilter implements Filter {
		
		@Override
		public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
				throws IOException, ServletException {
		
			AppController app = (AppController) ((HttpServletRequest) request).getSession()
					.getAttribute("appController");

			try {
				if (app != null 
						&& app.getUsuario() != null 
						&& app.getUsuario().getId() != null
						&& app.getUsuario().getStatus() != null 
						&& app.getUsuario().getStatus()
						&& (app.getUsuario().getJuridica().getClass().equals(Representante.class)||
							app.getUsuario().getJuridica().getClass().equals(Fornecedor.class))
						) {
					
					
					chain.doFilter(request, response);
				} else {

					
					String contextPath = ((HttpServletRequest) request).getContextPath();
					 ((HttpServletResponse)response).sendRedirect(contextPath+"/login");
				}
			} catch (Exception e) {
				e.printStackTrace();
				String contextPath = ((HttpServletRequest) request).getContextPath();
				 ((HttpServletResponse)response).sendRedirect(contextPath+"/login");

				
			}
		}
		
		@Override
		public void destroy() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void init(FilterConfig arg0) throws ServletException {
			// TODO Auto-generated method stub
			
		}


		
	}
