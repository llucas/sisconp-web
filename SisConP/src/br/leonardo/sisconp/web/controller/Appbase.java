package br.leonardo.sisconp.web.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Juridica;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@ManagedBean
public abstract class Appbase<T> {

	@ManagedProperty(value = "#{appController}")
	private AppController app;

	private T objeto;
	private List<T> lista;
	private DAO<T> dao;

	public abstract void init();

	public abstract Class<T> findClass();

	public Appbase() {
		super();
		newdao();
	}

	public DAO<T> newdao() {
		return dao = new DAO<T>(findClass());
	}

	public String incluir() {
		try {
			Class<T> c = findClass();
			objeto = (T) c.getConstructor().newInstance();
			setDefaults();
			setDefaults2();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return "form?faces-redirect=true";
	}

	public String erroIncluir(String redirect) {
		app.mensagens(null, FacesMessage.SEVERITY_WARN, "Voce nao pode Executar essa a��o", "");
		return redirect + "?faces-redirect=true";
	}

	public void setDefaults2() {
	}

	public void setDefaults() {

		try {
			findClass().getMethod("setStatus", Boolean.class).invoke(objeto, true);
			findClass().getMethod("setUsuario", Usuario.class).invoke(objeto, app.getUsuario());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

	}

	public String gravar() {
		if (app.getUsuario().getJuridica().equals(getPessoa())) {
			try {
				objeto = dao.merge(objeto);
				app.mensagens(null, FacesMessage.SEVERITY_INFO, "Registro Salvo", "");
				init();
				roitinaGravacao();
				return "list?faces-redirect=true";
			} catch (Exception e) {
				e.printStackTrace();
				app.mensagens(null, FacesMessage.SEVERITY_INFO, "Erro ao Salvar", "");
				return "form?faces-redirect=true";
			}
		} else {
			objeto = null;
			System.out.println("erro");
			app.mensagens(null, FacesMessage.SEVERITY_ERROR, "Ops", "Voc� n�o pode fazer isso");
			return "form?faces-redirect=true";
		}
	}
	
	public void roitinaGravacao(){
		
	}

	public String cancelar() {
		return "list";
	}

	public String alterar(Long id) {
		objeto = (T) dao.getInstance(id);
		if (app.getUsuario().getJuridica().equals(getPessoa())) {
			return "form?faces-redirect=true";
		} else {
			objeto = null;
			app.mensagens(null, FacesMessage.SEVERITY_ERROR, "Ops", "Voc� n�o pode fazer isso");
			return "list?faces-redirect=true";
		}

	}

	public String excluir(Long id) {
		dao.remove(id);
		return "list?faces-redirect=true";
	}

	public T getObjeto() {
		return objeto;
	}

	public void setObjeto(T objeto) {
		this.objeto = objeto;
	}

	public List<T> getLista() {
		return lista;
	}

	public void setLista(List<T> lista) {
		this.lista = lista;
	}

	public DAO<T> getDao() {
		return (DAO<T>) dao;
	}

	public void setDao(DAO<T> dao) {
		this.dao = dao;
	}

	public AppController getApp() {
		return app;
	}

	public void setApp(AppController app) {
		this.app = app;
	}

	public String status(Boolean bol) {
		if (bol) {
			return "Ativo";
		} else {
			return "Inativo";
		}
	}

	public abstract Pessoa getPessoa();

	public String inativar(Long id) {
		if (app.getUsuario().getJuridica().equals(getPessoa())) {
			objeto = (T) dao.getInstance(id);
			try {
				findClass().getMethod("setStatus", Boolean.class).invoke(objeto, false);
				dao.merge(objeto);
				app.mensagens(null, FacesMessage.SEVERITY_INFO, "Registro Inativado", " ");
			} catch (Exception e) {
				e.printStackTrace();
				app.mensagens(null, FacesMessage.SEVERITY_ERROR, "Ops", "Erro ao alterar");
			}
		} else {
			app.mensagens(null, FacesMessage.SEVERITY_ERROR, "Ops", "Voc� n�o pode fazer isso");
		}
		return "list?faces-redirect=true";
	}

}
