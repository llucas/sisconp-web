package br.leonardo.sisconp.web.controller.usuario;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@ManagedBean
@SessionScoped
public class CadastroController {

	private Usuario usuario;
	private String codigoValidacao;
	private String formClass = "fs-form fs-form-overview fs-show";
	private String bodyClass = "decor-primary o-visible overview";
	private Boolean erro = false;

	public void initNew() {

		usuario = new Usuario();

		// formClass="fs-form fs-form-overview fs-show";
		// bodyClass="decor-primary o-visible overview";
		// erro=false;
		//
		// formClass="fs-form fs-form-full";
		// bodyClass="decor-primary o-visible";
		// erro=true;

	}

	public String verificado() {
		return erro.toString();
	}

	public String cadastra() {

		try {
			usuario.setStatus(false);
			usuario.setSenha(usuario.getSenha().hashCode() + "");
			usuario.setCodigovalidacao(new Date().toString().hashCode() + "");
			enviaEmail(usuario.getCodigovalidacao());
			DAO<Usuario> daoUsuario = new DAO<Usuario>(Usuario.class);
			usuario=daoUsuario.merge(usuario);
			return "confimaremail?faces-redirect=true";
		} catch (EmailException e) {
			mensagens("erro", FacesMessage.SEVERITY_ERROR, "Dados invalidos, Verifique seu E-Mail e seus dados e tente novamente", "");
			e.printStackTrace();
			return "cadastro?faces-redirect=true";
		} catch (Exception e){
			mensagens("erro", FacesMessage.SEVERITY_ERROR, "Dados invalidos, Verifique seu E-Mail e seus dados e tente novamente", "");
			e.printStackTrace();
			return "cadastro?faces-redirect=true";
		}

		

	}

	public String validaEmail() {
		DAO<Usuario> daoUsuario = new DAO<Usuario>(Usuario.class);
		Usuario user;
		try {
			user = (Usuario) daoUsuario.atenticarCodigoValidacao(codigoValidacao);
			user.setStatus(true);
			daoUsuario.merge(user);
			return "login";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public String registraEmpresa() {
		DAO<Usuario> daoUsuario = new DAO<Usuario>(Usuario.class);

		try {
			// user=(Comum)daoUsuario.atenticarCodigoValidacao(codigoValidacao);
			// user.setStatus(true);
			//
			// //user.setJuridica(new Fornecedor( "", usuario.getNome(), "", "",
			// false, usuario.getEmail(), "", "", "", "", "", "", "", usuario,
			// "", "",null, usuario, null, null, null, null));
			//
			// daoUsuario.merge(user);
			return "registroempresa";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public Boolean enviaEmail() {
		return null;

	}

	public void mensagens(String id, Severity tipo, String mprincipal, String msecundario) {
		FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(tipo, mprincipal, msecundario));
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getCodigoValidacao() {
		return codigoValidacao;
	}

	public void setCodigoValidacao(String codigoValidacao) {
		this.codigoValidacao = codigoValidacao;
	}

	public String getFormClass() {
		return formClass;
	}

	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}

	public String getBodyClass() {
		return bodyClass;
	}

	public void setBodyClass(String bodyClass) {
		this.bodyClass = bodyClass;
	}

	public Boolean getErro() {
		return erro;
	}

	public void setErro(Boolean erro) {
		this.erro = erro;
	}

	public void enviaEmail(String codigo) throws EmailException {
		// Cria o e-mail
		HtmlEmail email = new HtmlEmail();

		email.setHostName("smtp.gmail.com");

		email.addTo(usuario.getEmail(), usuario.getNome());
		email.setSmtpPort(587);

		email.setFrom("sisconp.adm@gmail.com", "Sisconp");
		//
		email.setTLS(true);
		email.setSubject("Aviso Cadastro");

		email.setAuthentication("sisconp.adm@gmail.com", "sisconp-adm");
		String cabecalho = "";
		String corpo = "";
		String rodape = "";
		String tcabecalho = "";
		String tcorpo = "";
		String trodape = "";

		cabecalho = "<html><span style='font-weight: bold;'>Bem-vindo ao Sisconp</span>" + "<br><br>";

		corpo = "<br><br>Seu Login : " + usuario.getLogin() + "<br>Seu c�digo � : " + codigo;

		// configure uma mensagem alternativa caso o servidor no suporte
		// HTML
		tcabecalho = "Bem-vindo ao Sisconp,";
		tcorpo = "Seu Login : " + usuario.getLogin() + ", Seu c�digo � : " + codigo;

		email.setHtmlMsg(cabecalho + corpo + rodape);
		email.setTextMsg(tcabecalho + tcorpo + trodape);
		// envia o e-mail
		email.send();

	}

}
