package br.leonardo.sisconp.web.controller.usuario;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.web.controller.Appbase;
import br.leonardo.sisconp.web.controller.PasswordGenerator;


@ManagedBean
@SessionScoped
public class UsuarioController extends Appbase<Usuario> {
	
	private String Senha;
	
	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Usuario o where o.juridica.id="
				+ getApp().getUsuario().getJuridica().getId())); 
	}

	@Override
	public Class<Usuario> findClass() {
		return Usuario.class;
	}

	@Override
	public void setDefaults2(){
		getObjeto().setJuridica(getApp().getUsuario().getJuridica());
		Senha=PasswordGenerator.getRandomPassword();
		getObjeto().setSenha(Senha.hashCode()+"");
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getJuridica();
	}
	
	@Override
	public void roitinaGravacao(){
		try {
			enviaEmail(Senha);
		} catch (EmailException e) {
			getApp().mensagens("erro", FacesMessage.SEVERITY_ERROR, "Dados invalidos, Verifique seu E-Mail e seus dados e tente novamente", "");
			e.printStackTrace();
		}
	}
	
	
	
	public void enviaEmail(String codigo) throws EmailException {
		// Cria o e-mail
		HtmlEmail email = new HtmlEmail();

		email.setHostName("smtp.gmail.com");

		email.addTo(getObjeto().getEmail(), getObjeto().getNome());
		email.setSmtpPort(587);

		email.setFrom("sisconp.adm@gmail.com", "Sisconp");
		//
		email.setTLS(true);
		email.setSubject("Aviso Cadastro");

		email.setAuthentication("sisconp.adm@gmail.com", "sisconp-adm");
		String cabecalho = "";
		String corpo = "";
		String rodape = "";
		String tcabecalho = "";
		String tcorpo = "";
		String trodape = "";

		cabecalho = "<html><span style='font-weight: bold;'>Bem-vindo ao Sisconp</span>" + "<br><br>";

		corpo = "<br><br>Seu Login : " + getObjeto().getLogin() + "<br>Sua senha � : " + codigo;

		// configure uma mensagem alternativa caso o servidor no suporte
		// HTML
		tcabecalho = "Bem-vindo ao Sisconp,";
		tcorpo = "Seu Login : " + getObjeto().getLogin() + ", sua senha � : " + codigo;

		email.setHtmlMsg(cabecalho + corpo + rodape);
		email.setTextMsg(tcabecalho + tcorpo + trodape);
		// envia o e-mail
		email.send();

	}

}
