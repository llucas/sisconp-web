package br.leonardo.sisconp.web.controller.usuario;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.hibernate.validator.constraints.Length;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.web.controller.AppController;

@ManagedBean
@SessionScoped
public class PerfilController  {

	@ManagedProperty(value = "#{appController}")
	private AppController app;
	
	private String senha;
	
	@Length(min = 6, max = 22, message = "O campo nova senha deve ter entre {min} e {max} caracteres!")
	private String novasenha;
	private String confirmacaosenha;
	
	@PostConstruct
	public void init(){
		
	}
	
	
	public String gravar(){

		try {
			DAO<Usuario> daoUsuario= new DAO<Usuario>(Usuario.class);
			app.setUsuario(daoUsuario.merge(app.getUsuario()));
			FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard");
	
			return "/private/dashboard";	
		} catch (Exception e) {
			System.out.println("eRRO");
			e.printStackTrace();
			app.mensagens(null, FacesMessage.SEVERITY_WARN,"Erro ao gravar!", "");
			return "private/empresa?faces-redirect=true";	
		}
		
	}
	
	public String gravarSenha(){
		if(app.getUsuario().getSenha().equals(senha.hashCode()+"")&&novasenha.equals(confirmacaosenha)){
			try {
				app.getUsuario().setSenha(novasenha.hashCode()+"");
				DAO<Usuario> daoUsuario= new DAO<Usuario>(Usuario.class);
				app.setUsuario(daoUsuario.merge(app.getUsuario()));
				FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard");
				return "/private/dashboard";	
			} catch (Exception e) {
				e.printStackTrace();
				app.mensagens("errosenha", FacesMessage.SEVERITY_WARN,"Senha ou confirmação de senha incorretas!", "");
				return "private/perfil?faces-redirect=true";	
			}
		}else{
			app.mensagens("errosenha", FacesMessage.SEVERITY_WARN,"Senha ou confirmação de senha incorretas!", "");
			return "private/perfil?faces-redirect=true";	
		}
		

		
		
	}

	public AppController getApp() {
		return app;
	}

	public void setApp(AppController app) {
		this.app = app;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getNovasenha() {
		return novasenha;
	}


	public void setNovasenha(String novasenha) {
		this.novasenha = novasenha;
	}


	public String getConfirmacaosenha() {
		return confirmacaosenha;
	}


	public void setConfirmacaosenha(String confirmacaosenha) {
		this.confirmacaosenha = confirmacaosenha;
	}

	

}
