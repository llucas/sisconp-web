package br.leonardo.sisconp.web.controller.pedido;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pedido.ItemPedido;
import br.leonardo.sisconp.entity.pedido.Operacao;
import br.leonardo.sisconp.entity.pedido.Pedido;
import br.leonardo.sisconp.entity.pessoa.ClienteFinal;
import br.leonardo.sisconp.entity.pessoa.Desconto;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Representante;
import br.leonardo.sisconp.entity.pessoa.Revenda;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.entity.produto.Item;
import br.leonardo.sisconp.web.controller.Appbase;


@ManagedBean
@SessionScoped
public class PedidoController extends Appbase<Pedido> {
	
	private List<Revenda> revendas;
	private List<ClienteFinal> clientes;
	private List<Operacao> operacoes;
	private List<Item> itens;
	

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Pedido o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
		
		DAO<Revenda> revendaDao=new DAO<Revenda>(Revenda.class);
		
		revendas=revendaDao.getInstanceList("from Revenda o where o.representante.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
		
		DAO<Operacao> operacaoDao=new DAO<Operacao>(Operacao.class);
		
		operacoes=operacaoDao.getInstanceList("from Operacao o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
		
		DAO<Item> itemDao=new DAO<Item>(Item.class);
		
		itens=itemDao.getInstanceList("from Item o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
	}

	@Override
	public Class<Pedido> findClass() {
		return Pedido.class;
	}

	@Override
	public void setDefaults2(){
		
		
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getFornecedor();
	}

	//itens
	
	private ItemPedido itemPedido; // itemPedido em edio, vinculado ao formulrio
	private Integer rowIndex = null; // ndice do itemPedido selecionado - alterar e
										// excluir
	public void incluirItem() {
		rowIndex = null;
		itemPedido = new ItemPedido();
	}
	public void alterarItem(Integer rowIndex) {
		this.rowIndex = rowIndex;
		itemPedido = getObjeto().getItemsPedido().get(rowIndex); // pega itemPedido da
															// coleo
	}
	public void excluirItem(Integer rowIndex) {
		getObjeto().getItemsPedido().remove(rowIndex.intValue()); // exclui itemPedido
		
	}
	public void gravarItem() {
		if (this.rowIndex == null) {
			itemPedido.setPedido(getObjeto());
			getObjeto().getItemsPedido().add(itemPedido); // adiciona itemPedido na coleo
		} else {
			getObjeto().getItemsPedido().set(rowIndex, itemPedido); // altera na
																// coleo
		}
		rowIndex = null;
		itemPedido = null;
	}
	public void cancelarItem() {
		rowIndex = null;
		itemPedido = null;
	}
	

	
	
	
	public List<Revenda> getRevendas() {
		return revendas;
	}

	public void setRevendas(List<Revenda> revendas) {
		this.revendas = revendas;
	}

	public List<ClienteFinal> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteFinal> clientes) {
		this.clientes = clientes;
	}

	public List<Operacao> getOperacoes() {
		return operacoes;
	}

	public void setOperacoes(List<Operacao> operacoes) {
		this.operacoes = operacoes;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public ItemPedido getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(ItemPedido itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Integer getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
	
	
	



}
