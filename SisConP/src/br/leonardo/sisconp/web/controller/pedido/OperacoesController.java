package br.leonardo.sisconp.web.controller.pedido;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.entity.pedido.Operacao;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.web.controller.Appbase;


@ManagedBean
@SessionScoped
public class OperacoesController extends Appbase<Operacao> {
	
	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Operacao o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
	}

	@Override
	public Class<Operacao> findClass() {
		return Operacao.class;
	}

	@Override
	public void setDefaults2(){
		getObjeto().setFornecedor((Fornecedor) getApp().getUsuario().getJuridica());
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getFornecedor();
	}
	
	
	

}
