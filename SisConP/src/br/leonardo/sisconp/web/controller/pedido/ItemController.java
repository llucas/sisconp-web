package br.leonardo.sisconp.web.controller.pedido;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.produto.Item;
import br.leonardo.sisconp.web.controller.Appbase;

@ManagedBean
@SessionScoped
public class ItemController extends Appbase<Item> {

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Item o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
	}

	@Override
	public Class<Item> findClass() {
		return Item.class;
	}

	@Override
	public void setDefaults2(){
		getObjeto().setFornecedor((Fornecedor) getApp().getUsuario().getJuridica());
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getFornecedor();
	}
}
