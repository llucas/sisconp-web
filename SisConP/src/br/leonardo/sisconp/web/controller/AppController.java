package br.leonardo.sisconp.web.controller;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@ManagedBean
@SessionScoped
public class AppController {

	private String pageclass;

	private String senha;

	private String login;

	private String novaSenha;

	private String senhaAtual;

	private Usuario usuario;

	public void init() {
		login = "";
		senha = "";

	}

	public String doLogin() {
		try {
			usuario = new DAO<Usuario>(Usuario.class).atenticarUsuario(login, senha.hashCode() + "");
			if (usuario == null) {
				mensagens(null, FacesMessage.SEVERITY_WARN, "Login e/ou Senha Incorretos", " ");
				senha = "";
				return "login";
			} else if (!usuario.getStatus()) {
				usuario = null;
				senha = "";
				mensagens(null, FacesMessage.SEVERITY_WARN, "Usurio inativo", " Contade seu representante");
				return "login";
			} else {
				login = "";
				senha = "";
				if ((usuario.getJuridica() == null || usuario.getJuridica().getId() == null)) {
					return "private/empresa?faces-redirect=true";
				} else {
					return "private/dashboard?faces-redirect=true";
				}

			}

		} catch (Exception e) {
			login = "";
			senha = "";
			e.printStackTrace();
			mensagens(null, FacesMessage.SEVERITY_ERROR, "Login e/ou Senha Incorretos", " ");

			return "login";
		}

	}

	public String doLogout() {
		usuario = null;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		new FacesMessage();
		mensagens(null, FacesMessage.SEVERITY_INFO, "Usurio Desconectado!", "");
		return "/login";
	}

	public String doHome() {
		new FacesMessage();
		mensagens(null, FacesMessage.SEVERITY_INFO, "Usurio sem permisso!", "");
		return "dashboard";
	}

	public void mensagens(String id, Severity tipo, String mprincipal, String msecundario) {
		FacesContext.getCurrentInstance().addMessage(id, new FacesMessage(tipo, mprincipal, msecundario));
	}

	public String getPageclass() {
		return pageclass;
	}

	public void setPageclass(String pageclass) {
		this.pageclass = pageclass;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
