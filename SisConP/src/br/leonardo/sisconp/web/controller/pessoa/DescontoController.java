package br.leonardo.sisconp.web.controller.pessoa;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.entity.pessoa.Desconto;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.web.controller.Appbase;

@ManagedBean
@SessionScoped
public class DescontoController extends Appbase<Desconto>{

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Desconto o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
	}

	@Override
	public Class<Desconto> findClass() {
		return Desconto.class;
	}

	@Override
	public void setDefaults2(){
		getObjeto().setFornecedor((Fornecedor) getApp().getUsuario().getJuridica());
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getFornecedor();
	}
	

}
