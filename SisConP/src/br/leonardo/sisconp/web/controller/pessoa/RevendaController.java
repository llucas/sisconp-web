package br.leonardo.sisconp.web.controller.pessoa;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Desconto;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Representante;
import br.leonardo.sisconp.entity.pessoa.Revenda;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.web.controller.Appbase;

@ManagedBean
@SessionScoped
public class RevendaController extends Appbase<Revenda>{

	private List<Representante> representantes;
	
	private List<Desconto> descontos;

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Revenda o where o.representante.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
		
		DAO<Representante> representanteDao=new DAO<Representante>(Representante.class);
		
		representantes=representanteDao.getInstanceList("from Representante o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
		
		
		DAO<Desconto> descontoDao=new DAO<Desconto>(Desconto.class);
		
		descontos=descontoDao.getInstanceList("from Desconto o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
		
	}

	@Override
	public Class<Revenda> findClass() {
		return Revenda.class;
	}

	@Override
	public void setDefaults2(){
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyHHmmss");
		getObjeto().setAdministrador(new Usuario(null,dt.format(new Date())+getApp().getUsuario().getId(), dt.format(new Date())+getApp().getUsuario().getId(), "Seu Nome", null, "email@mail.com", false, getApp().getUsuario(), getObjeto()));
	
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getRepresentante().getFornecedor();
	}
	
	//geters e stters


	public List<Representante> getRepresentantes() {
		return representantes;
	}

	public void setRepresentantes(List<Representante> representantes) {
		this.representantes = representantes;
	}

	public List<Desconto> getDescontos() {
		return descontos;
	}

	public void setDescontos(List<Desconto> descontos) {
		this.descontos = descontos;
	}

	
	
	
	

}
