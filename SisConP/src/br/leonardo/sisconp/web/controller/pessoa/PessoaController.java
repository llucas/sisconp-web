package br.leonardo.sisconp.web.controller.pessoa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Juridica;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.web.controller.AppController;

@ManagedBean
@SessionScoped
public class PessoaController {
	
	@ManagedProperty(value = "#{appController}")
	private AppController app;
	
	@PostConstruct
	public void init(){
		if(app.getUsuario().getJuridica()!=null){
			if(app.getUsuario().getJuridica().getId()==null){
			app.getUsuario().setJuridica(new Fornecedor());
			}
		}else{
			System.out.println("Criou novo");
			app.getUsuario().setJuridica(new Fornecedor());
		}
	}
	
	public String gravar(){

		try {
			if(app.getUsuario().getJuridica().getStatus()==null){
				app.getUsuario().getJuridica().setUsuario(app.getUsuario());
				app.getUsuario().getJuridica().setStatus(true);
				if(app.getUsuario().getJuridica().getClass().equals(Fornecedor.class)){
					app.getUsuario().getJuridica().setAdministrador(app.getUsuario());
				}
			}
			DAO<Usuario> daoUsuario= new DAO<Usuario>(Usuario.class);
			app.setUsuario(daoUsuario.merge(app.getUsuario()));
			FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard");
	
			return "/private/dashboard";	
		} catch (Exception e) {
			e.printStackTrace();
			app.mensagens(null, FacesMessage.SEVERITY_WARN,"Erro ao gravar!", "");
			return "private/empresa?faces-redirect=true";	
		}
		
	}

	public AppController getApp() {
		return app;
	}

	public void setApp(AppController app) {
		this.app = app;
	}

	
	
//	upload
	
//
//	public void upload(FileUploadEvent event) {  
//		 try {
//			 System.out.println("01");
//           copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
//           
//       } catch (Exception e) {
//    	   System.out.println("02");
//           e.printStackTrace();
//       }
//		 
//	 }
//	private void copyFile(String fileName, InputStream in) throws Exception {
//		 System.out.println("0u");
////		monta data
//		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyyhhmmss");
//		String arquivo=dt.format(new Date());
////		pega empresa
////		deletar o arquivo antigo
//		String caminho=FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
//		try {
//			File fileantigo = new File(caminho,((Juridica)pessoa).getLogo());
//			if(fileantigo.delete())
//				app.mensagens(null, FacesMessage.SEVERITY_WARN, "Imagem antiga excluda", "");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		caminho
//		caminho+="public/img/pessoa/";
////		nome minusculo
//		fileName=fileName.toLowerCase();
//		
////		montagem do nome
//		if(fileName.contains(".png")){
//			arquivo=arquivo+".png";
//		}else if (fileName.contains(".jpg")) {
//			arquivo=arquivo+".jpg";
//		}else if (fileName.contains(".jpeg")) {
//			arquivo=arquivo+".jpeg";
//		}
//		caminho+=arquivo;
//		
//		
//		if(fileName.contains("png")||fileName.contains("jpg")||fileName.contains("jpeg")){
//			File filee = new File(caminho);
//			
//			FileOutputStream fout = new FileOutputStream(filee);
//		
//			while (in.available() != 0) {
//				fout.write(in.read());
//			}
//			
//			fout.close();
//		
//			if(pessoa.getClass().equals(Juridica.class)){
//				((Juridica)pessoa).setLogo("public/img/pessoa/"+arquivo);
//			}
//			
//			app.mensagens(null, FacesMessage.SEVERITY_INFO, "Imagen Salva", "");
//		}else{
//			app.mensagens(null, FacesMessage.SEVERITY_ERROR, "Arquivo Invalido", " ");
//		}
//	}
//
////	end Upload
	
	
	

}
