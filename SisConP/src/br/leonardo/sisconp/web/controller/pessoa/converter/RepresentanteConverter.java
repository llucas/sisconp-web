package br.leonardo.sisconp.web.controller.pessoa.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Representante;

@FacesConverter(forClass = Representante.class, value = "representanteConverter")
public class RepresentanteConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0) {
			try {
				if (value == null || value.isEmpty()) {
					return null;
				} else {
					Long id = Long.parseLong(value);
					Representante obj = (Representante) new DAO<Representante>(Representante.class).getInstance(id);
					return obj;
				}

			} catch (NumberFormatException e) {
				e.printStackTrace();
				throw new ConverterException(
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro de Converso da Pessoa", "."));
			}
		} else{
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((Representante) object).getId());
		} else
			return null;
	}
}