package br.leonardo.sisconp.web.controller.pessoa;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.ClienteFinal;
import br.leonardo.sisconp.entity.pessoa.Fisica;
import br.leonardo.sisconp.entity.pessoa.Juridico;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Revenda;
import br.leonardo.sisconp.web.controller.Appbase;

@ManagedBean
@SessionScoped
public class ClienteController extends Appbase<ClienteFinal>{

	private List<Revenda> revendas;

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from ClienteFinal o where o.revenda.representante.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
		
		DAO<Revenda> revendaDao=new DAO<Revenda>(Revenda.class);
		
		revendas=revendaDao.getInstanceList("from Revenda o where o.representante.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId()); 
	}

	@Override
	public Class<ClienteFinal> findClass() {
		return ClienteFinal.class;
	}

	@Override
	public void setDefaults2(){
		
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getRevenda().getRepresentante().getFornecedor();
	}
	
	

	@Override
	public String incluir() {return "";}
	
	public String incluirJuridica(){
		try {
			setObjeto(new Juridico());
			setDefaults();
			setDefaults2();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return "form?faces-redirect=true";
	}
	
	public String incluirFisica(){
		try {
			setObjeto(new Fisica());
			setDefaults();
			setDefaults2();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return "form?faces-redirect=true";
	}
	
	
	//geters e stters

	public List<Revenda> getRevendas() {
		return revendas;
	}

	public void setRevendas(List<Revenda> revendas) {
		this.revendas = revendas;
	}
	
	
	
	
	
	

}
