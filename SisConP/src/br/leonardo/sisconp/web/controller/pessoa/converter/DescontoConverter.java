package br.leonardo.sisconp.web.controller.pessoa.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Desconto;
import br.leonardo.sisconp.entity.pessoa.Representante;

@FacesConverter(value = "descontoConverter")
public class DescontoConverter implements Converter {
	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if (value != null && value.trim().length() > 0) {
			try {
				if (value == null || value.isEmpty()) {
					return null;
				} else {
					Long id = Long.parseLong(value);
					Desconto obj = (Desconto) new DAO<Desconto>(Desconto.class).getInstance(id);
					return obj;
				}

			} catch (NumberFormatException e) {
				e.printStackTrace();
				throw new ConverterException(
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao Converter", "."));
			}
		} else{
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != null) {
			return String.valueOf(((Desconto) object).getId());
		} else
			return null;
	}
}