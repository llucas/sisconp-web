package br.leonardo.sisconp.web.controller.pessoa;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Pessoa;
import br.leonardo.sisconp.entity.pessoa.Representante;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.web.controller.Appbase;

@ManagedBean
@SessionScoped
public class RepresentanteController extends Appbase<Representante>{

	@PostConstruct
	@Override
	public void init() {
		setLista(getDao().getInstanceList("from Representante o where o.fornecedor.id="
				+ getApp().getUsuario().getJuridica().getId())); 
		
	}

	@Override
	public Class<Representante> findClass() {
		return Representante.class;
	}

	@Override
	public void setDefaults2(){
		getObjeto().setFornecedor((Fornecedor) getApp().getUsuario().getJuridica());
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyHHmmss");
		getObjeto().setAdministrador(new Usuario(null,dt.format(new Date())+getApp().getUsuario().getId(), dt.format(new Date())+getApp().getUsuario().getId(), "Seu Nome", null, "email@mail.com", false, getApp().getUsuario(), getObjeto()));
	}

	@Override
	public Pessoa getPessoa() {
		return getObjeto().getFornecedor();
	}
	
	
	
	
	

}
