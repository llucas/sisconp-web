package br.leonardo.sisconp.conexao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.leonardo.sisconp.entity.pessoa.Usuario;


public class DAO<T> {

	private Class classe;

	public DAO(Class classe) {
		super();
		this.classe = classe;
	}
	
	
	
	
	public Usuario atenticarUsuario(String login, String senha) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			String hql = "from Usuario u WHERE u.login = :login and u.senha = :senha";
			Query q= em.createQuery(hql);
			q.setParameter("login", login);
			q.setParameter("senha", senha);
			
			return (Usuario) q.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	
	}
	
	
	public Usuario atenticarCodigoValidacao(String codigovalidacao) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			String hql = "from Usuario u WHERE u.codigovalidacao = :codigovalidacao";
			Query q= em.createQuery(hql);
			q.setParameter("codigovalidacao", codigovalidacao);
			
			return (Usuario) q.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	
	}
	
	
	
	

	/**
	 *  
	 */
	public T merge(T instancia){
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			em.getTransaction().begin();
			instancia = em.merge(instancia);
			em.getTransaction().commit();
			return instancia;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 *  
	 */
	public void remove(T instancia) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			em.getTransaction().begin();
			instancia = em.merge(instancia);
			em.remove(instancia);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
	}

	public void remove(Long id) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(em.find(classe, id));
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
	}

	public Object getInstance(Long id) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			return (T) em.find(classe, id);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
	}
	
	
	
//	public static Object getInstance(Long id,Class Classeob) {
//		EntityManager em = FabricaConexao.getEntityManager();
//		try {
//			return em.find(Classeob, id);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			em.close();
//		}
//	}
//	public Usuario atenticarUsuario(String login, String senha) {
//		EntityManager em = FabricaConexao.getEntityManager();
//		try {
//			String hql = "from Usuario u WHERE u.login = :login and u.senha = :senha";
//			Query q= em.createQuery(hql);
//			q.setParameter("login", login);
//			q.setParameter("senha", senha);
//			
//			return (Usuario) q.getSingleResult();
//		} catch (Exception e) {
//			return null;
//		} finally {
//			em.close();
//		}
//	
//	}

	public List<T> getInstanceList() {
		List<T> retorno = new ArrayList<T>();
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			String hql = "from " + classe.getSimpleName() + " ";
			retorno = em.createQuery(hql).getResultList();
		} catch (Exception e) {
			
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
		return retorno;
	}
	
	public List<T> getInstanceList(String hql) {
		List<T> retorno = new ArrayList<T>();
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			retorno = em.createQuery(hql).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
		return retorno;
	}
	
	public T getInstance(String hql) {
		EntityManager em = FabricaConexao.getEntityManager();
		try {
			return (T) em.createQuery(hql).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
		
		
	}
	
}
