package br.leonardo.sisconp.log;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "log")
public class Log {
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_log")
	@SequenceGenerator(name = "seq_log", sequenceName = "seq_log", allocationSize = 1, initialValue = 1, schema = "log")
	private Long id;
	@Column(length = 100)
	private String tabela;
	@Column(length = 1)
	private String acao;
	@Column(length = 5000)
	private String original;
	@Column(length = 5000)
	private String novo;
	@Column(length = 5000)
	private String query;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTabela() {
		return tabela;
	}
	public void setTabela(String tabela) {
		this.tabela = tabela;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getOriginal() {
		return original;
	}
	public void setOriginal(String original) {
		this.original = original;
	}
	public String getNovo() {
		return novo;
	}
	public void setNovo(String novo) {
		this.novo = novo;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	
	
 	

}
