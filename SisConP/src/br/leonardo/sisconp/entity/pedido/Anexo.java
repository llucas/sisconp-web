package br.leonardo.sisconp.entity.pedido;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.leonardo.sisconp.entity.pessoa.Usuario;

@Entity
public class Anexo{
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_anexo")
	@SequenceGenerator(name = "seq_anexo", sequenceName = "seq_anexo", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAnexo;

	@Length(min = 3, max = 100, message = "A descricao deve ter entre {min} e {max} caracteres!")
	@Column(length = 100)
	private String descricao;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(fetch = LAZY, optional = false)
	private Pedido pedido;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false, fetch = LAZY)
	private Usuario usuario;
	
	public Anexo() {
		dataAnexo=new Date();
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataAnexo() {
		return dataAnexo;
	}

	public void setDataAnexo(Date dataAnexo) {
		this.dataAnexo = dataAnexo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anexo other = (Anexo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
