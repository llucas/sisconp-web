package br.leonardo.sisconp.entity.pedido;

import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.InheritanceType.JOINED;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Representante;
import br.leonardo.sisconp.entity.pessoa.Revenda;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@Entity
@Inheritance(strategy = JOINED)
public class Pedido {
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_pedido")
	@SequenceGenerator(name = "seq_pedido", sequenceName = "seq_pedido", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@Column(nullable = false)
	private Boolean status;
	
	@Column(nullable = false)
	private Boolean irregular;
	
	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double valorFreteTotal;

	
	@Column(nullable = false)
	private Boolean fretePago;

	@Past(message="A data de efetiva��o deve estar no passado")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEfetivacao;
	
	@Future(message="A data de efetiva��o deve estar no futuro")
	@Column(nullable = true)
	private Date validade;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double valorTotal;

	
	@Column(nullable = true)
	private Double valorTotalFinal;

	
	@OneToMany(orphanRemoval = true, mappedBy = "pedido")
	private List<ItemPedido> itemsPedido;

	@Column(nullable = true)
	private String parcelamento;
	
	@ManyToOne(optional = true)
	private Representante representante;

	@ManyToOne(optional = true)
	private Revenda revenda;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Operacao operacao;

	@OneToMany(orphanRemoval = true, mappedBy = "pedido")
	private List<Anexo> anexo;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = true)
	private Fornecedor fornecedor;
	
	@Column(nullable = true)
	private String observacao;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean getIrregular() {
		return irregular;
	}

	public void setIrregular(Boolean irregular) {
		this.irregular = irregular;
	}

	public Double getValorFreteTotal() {
		return valorFreteTotal;
	}

	public void setValorFreteTotal(Double valorFreteTotal) {
		this.valorFreteTotal = valorFreteTotal;
	}

	public Boolean getFretePago() {
		return fretePago;
	}

	public void setFretePago(Boolean fretePago) {
		this.fretePago = fretePago;
	}

	public Date getDataEfetivacao() {
		return dataEfetivacao;
	}

	public void setDataEfetivacao(Date dataEfetivacao) {
		this.dataEfetivacao = dataEfetivacao;
	}

	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Double getValorTotalFinal() {
		return valorTotalFinal;
	}

	public void setValorTotalFinal(Double valorTotalFinal) {
		this.valorTotalFinal = valorTotalFinal;
	}

	public List<ItemPedido> getItemsPedido() {
		return itemsPedido;
	}

	public void setItemsPedido(List<ItemPedido> itemsPedido) {
		this.itemsPedido = itemsPedido;
	}


	public String getParcelamento() {
		return parcelamento;
	}

	public void setParcelamento(String parcelamento) {
		this.parcelamento = parcelamento;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}

	public List<Anexo> getAnexo() {
		return anexo;
	}

	public void setAnexo(List<Anexo> anexo) {
		this.anexo = anexo;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
