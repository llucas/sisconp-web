package br.leonardo.sisconp.entity.pedido;


import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;

@Entity
public class Operacao {
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_operacao")
	@SequenceGenerator(name = "seq_operacao", sequenceName = "seq_operacao", allocationSize = 1, initialValue = 1)
	private Long id;

	@Length(min=2, max=100, message="O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 100)
	private String descricao;

	@Column(nullable = false)
	private Boolean parcelas;

	@Column(nullable = false)
	private Boolean comissao;

	@Column(nullable = false)
	private Boolean frete;
	
	@Column(nullable = false)
	private Boolean status;

	@Length(min=8, max=8, message="O campo deve ter {min} digitos, e sem simbolos!")
	@Column(nullable = true)
	private String cfop;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	private Usuario usuario;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false, cascade = { PERSIST, MERGE, REFRESH })
	private Fornecedor fornecedor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean getParcelas() {
		return parcelas;
	}

	public void setParcelas(Boolean parcelas) {
		this.parcelas = parcelas;
	}

	public Boolean getComissao() {
		return comissao;
	}

	public void setComissao(Boolean comissao) {
		this.comissao = comissao;
	}

	public Boolean getFrete() {
		return frete;
	}

	public void setFrete(Boolean frete) {
		this.frete = frete;
	}

	public String getCfop() {
		return cfop;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operacao other = (Operacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

	
	
}
