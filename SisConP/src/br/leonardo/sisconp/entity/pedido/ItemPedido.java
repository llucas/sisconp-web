package br.leonardo.sisconp.entity.pedido;


import static javax.persistence.GenerationType.SEQUENCE;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.entity.produto.Baixa;
import br.leonardo.sisconp.entity.produto.Item;

@Entity
public class ItemPedido {
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_item_pedido")
	@SequenceGenerator(name = "seq_item_pedido", sequenceName = "seq_item_pedido", allocationSize = 1, initialValue = 1)
	private Long id;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Double quantidade;
	
	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Integer previsaoEntrega;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double valorClienteFinal;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Double valorRevenda;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double freteTotal;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double freteUnitario;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Pedido pedido;
	
	@OneToMany(orphanRemoval = true, mappedBy = "itemPedido")
	private List<Baixa> baixas;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Item item;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getPrevisaoEntrega() {
		return previsaoEntrega;
	}

	public void setPrevisaoEntrega(Integer previsaoEntrega) {
		this.previsaoEntrega = previsaoEntrega;
	}

	public Double getValorClienteFinal() {
		return valorClienteFinal;
	}

	public void setValorClienteFinal(Double valorClienteFinal) {
		this.valorClienteFinal = valorClienteFinal;
	}

	public Double getValorRevenda() {
		return valorRevenda;
	}

	public void setValorRevenda(Double valorRevenda) {
		this.valorRevenda = valorRevenda;
	}

	public Double getFreteTotal() {
		return freteTotal;
	}

	public void setFreteTotal(Double freteTotal) {
		this.freteTotal = freteTotal;
	}

	public Double getFreteUnitario() {
		return freteUnitario;
	}

	public void setFreteUnitario(Double freteUnitario) {
		this.freteUnitario = freteUnitario;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public List<Baixa> getBaixas() {
		return baixas;
	}

	public void setBaixas(List<Baixa> baixas) {
		this.baixas = baixas;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	

}
