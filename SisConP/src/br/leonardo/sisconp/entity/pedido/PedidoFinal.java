package br.leonardo.sisconp.entity.pedido;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import br.leonardo.sisconp.entity.pessoa.ClienteFinal;

@Entity
public class PedidoFinal extends Pedido {
	
	
	@ManyToOne(optional = true)
	private ClienteFinal clienteFinal;

	public ClienteFinal getClienteFinal() {
		return clienteFinal;
	}

	public void setClienteFinal(ClienteFinal clienteFinal) {
		this.clienteFinal = clienteFinal;
	}


	
}
