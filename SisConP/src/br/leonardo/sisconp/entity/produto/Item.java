package br.leonardo.sisconp.entity.produto;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@Entity
public class Item {
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_item")
	@SequenceGenerator(name = "seq_item", sequenceName = "seq_item", allocationSize = 1, initialValue = 1)
	private Long id;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Integer previsaoEntrega;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = true)
	private Double peso;
	
	@Length(min = 1, max = 10, message = "O campo deve ter entre {min} e {max} caracteres!")
	@Column(length = 10, nullable = true)
	private String ncm;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = false)
	private Double ipi;

	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Boolean status;

	
	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 500)
	private String descricao;

	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	private Double disponivelEstoque;

	@NotNull( message="O valor n�o pode ser nulo!")
	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = false)
	private Double preco;

	@Length(min = 2, max = 30, message = "O campo deve ter entre {min} e {max} caracteres!")
	@Column(nullable = true, length = 30)
	private String codigo;

	@Length(max = 1000, message = "O campo deve ter no m�ximo {max} caracteres!")
	@Column(nullable = true, length = 1000)
	private String observacao;

	@Length(max = 1000, message = "O campo deve ter no m�ximo {max} caracteres!")
	@Column(length = 3500)
	private String texto;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne
	private Fornecedor fornecedor;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPrevisaoEntrega() {
		return previsaoEntrega;
	}

	public void setPrevisaoEntrega(Integer previsaoEntrega) {
		this.previsaoEntrega = previsaoEntrega;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public Double getIpi() {
		return ipi;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getDisponivelEstoque() {
		return disponivelEstoque;
	}

	public void setDisponivelEstoque(Double disponivelEstoque) {
		this.disponivelEstoque = disponivelEstoque;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}


	public Fornecedor getFornecedores() {
		return fornecedor;
	}

	public void setFornecedores(Fornecedor fornecedor) {
		this.fornecedor= fornecedor;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
