package br.leonardo.sisconp.entity.produto;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.leonardo.sisconp.entity.pedido.ItemPedido;
import br.leonardo.sisconp.entity.pessoa.Usuario;

@Entity
public class Baixa{

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_baixa")
	@SequenceGenerator(name = "seq_baixa", sequenceName = "seq_baixa", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(nullable = true)
	private String chaveNf;

	@NotNull( message="O valor n�o pode ser nulo!")
	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@Column(nullable = false)
	private Double quantidade;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private ItemPedido itemPedido;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChaveNf() {
		return chaveNf;
	}

	public void setChaveNf(String chaveNf) {
		this.chaveNf = chaveNf;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public ItemPedido getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(ItemPedido itemPedido) {
		this.itemPedido = itemPedido;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Baixa other = (Baixa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
