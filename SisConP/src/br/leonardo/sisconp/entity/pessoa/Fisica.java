package br.leonardo.sisconp.entity.pessoa;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;

@Entity
public class Fisica extends ClienteFinal {

	@NotNull( message="O valor n�o pode ser nulo!")
	@CPF(message="O campo deve conter um CPF valido!")
	@Column(nullable = false, length = 14)
	private String doc;

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	

}
