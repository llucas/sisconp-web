package br.leonardo.sisconp.entity.pessoa;


import java.util.List;

import javax.persistence.Entity;

import br.leonardo.sisconp.entity.pedido.Pedido;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Revenda extends  Juridica {

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Desconto desconto;

	@OneToMany(mappedBy = "revenda")
	private List<Pedido> pedidos;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Representante representante;

	@OneToMany(mappedBy = "revenda")
	private List<ClienteFinal> clientesFinais;

	public Desconto getDesconto() {
		return desconto;
	}

	public void setDesconto(Desconto desconto) {
		this.desconto = desconto;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public List<ClienteFinal> getClientesFinais() {
		return clientesFinais;
	}

	public void setClientesFinais(List<ClienteFinal> clientesFinais) {
		this.clientesFinais = clientesFinais;
	}

	
}
