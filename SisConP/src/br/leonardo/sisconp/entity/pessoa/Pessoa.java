package br.leonardo.sisconp.entity.pessoa;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;

import static javax.persistence.InheritanceType.JOINED;

@Entity
@Inheritance(strategy = JOINED)
public abstract class Pessoa{
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_pessoa")
	@SequenceGenerator(name = "seq_pessoa", sequenceName = "seq_pessoa", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 150)
	private String endereco;

	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 150)
	private String nome;

	@Length(min = 5, max = 30, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(length = 30)
	private String inscricao;
	
	@Length(min = 3, max = 10, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = true, length = 10)
	private String numero;

	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Boolean status;
	
	@Length(min = 3, max = 255, message = "O campo deve contar um E-Mail valido!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Email(message="O campo deve contar um E-Mail valido!")
	@Column(nullable = true)
	private String email;

	@Length(min = 14, max = 14, message = "O campo deve ter {min} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 14)
	private String telefone;

	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 150)
	private String bairro;

	@Length(min = 8, max = 8, message = "O campo deve ter {min} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 8)
	private String cep;

	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 150)
	private String cidade;

	@Length(min = 2, max = 2, message = "O campo deve ter {min} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 2)
	private String estado;
	
	@Column(nullable = true)
	private String ibge;

	@Column(nullable = true)
	private String complemento;
	
	@Column(nullable = true)
	private String observacao;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getInscricao() {
		return inscricao;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIbge() {
		return ibge;
	}

	public void setIbge(String ibge) {
		this.ibge = ibge;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Pessoa( String endereco, String nome, String inscricao, String numero, Boolean status, String email,
			String telefone, String bairro, String cep, String cidade, String estado, String ibge, String observacao,
			Usuario usuario) {
		super();
		this.endereco = endereco;
		this.nome = nome;
		this.inscricao = inscricao;
		this.numero = numero;
		this.status = status;
		this.email = email;
		this.telefone = telefone;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.ibge = ibge;
		this.observacao = observacao;
		this.usuario = usuario;
	}

	public Pessoa() {
		super();
	}
	
	
	
	

}
