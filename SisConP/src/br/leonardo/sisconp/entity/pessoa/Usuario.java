package br.leonardo.sisconp.entity.pessoa;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
public class Usuario{

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_usuario")
	@SequenceGenerator(name = "seq_usuario", sequenceName = "seq_usuario", allocationSize = 1, initialValue = 1)
	private Long id;

	@Length(min = 6, max = 30, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 30)
	private String senha;

	@Length(min = 4, max = 30, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 30)
	private String login;

	@Length(min = 3, max = 150, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false, length = 150)
	private String nome;
	
	@Column(nullable = true)
	private String codigovalidacao;
	
	@Email(message="O campo deve contar um E-Mail valido!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private String email;

	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Boolean status;
	
	
	@ManyToOne(optional = true)
	private Usuario usuario;
	
	@ManyToOne(optional = true, fetch = EAGER, cascade = ALL)
	private Juridica juridica;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}


	public String getCodigovalidacao() {
		return codigovalidacao;
	}

	public void setCodigovalidacao(String codigovalidacao) {
		this.codigovalidacao = codigovalidacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Juridica getJuridica() {
		return juridica;
	}

	public void setJuridica(Juridica juridica) {
		this.juridica = juridica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Usuario(Long id, String senha, String login, String nome, String codigovalidacao, String email,
			Boolean status, Usuario usuario, Juridica juridica) {
		super();
		this.id = id;
		this.senha = senha;
		this.login = login;
		this.nome = nome;
		this.codigovalidacao = codigovalidacao;
		this.email = email;
		this.status = status;
		this.usuario = usuario;
		this.juridica = juridica;
	}

	public Usuario() {
		super();
	}
	
	
	

	
}
