package br.leonardo.sisconp.entity.pessoa;

import br.leonardo.sisconp.entity.pedido.PedidoFinal;

import static javax.persistence.InheritanceType.JOINED;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = JOINED)
public abstract class ClienteFinal extends Pessoa {

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Revenda revenda;

	@OneToMany(orphanRemoval = false, mappedBy = "clienteFinal")
	private List<PedidoFinal> pedidosFinais;

	public Revenda getRevenda() {
		return revenda;
	}

	public void setRevenda(Revenda revenda) {
		this.revenda = revenda;
	}

	public List<PedidoFinal> getPedidosFinais() {
		return pedidosFinais;
	}

	public void setPedidosFinais(List<PedidoFinal> pedidosFinais) {
		this.pedidosFinais = pedidosFinais;
	}

	
}
