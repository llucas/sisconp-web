package br.leonardo.sisconp.entity.pessoa;



import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

@Entity
public class Desconto{
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "seq_desconto")
	@SequenceGenerator(name = "seq_desconto", sequenceName = "seq_desconto", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@Length(min = 3, max = 100, message = "O campo deve ter entre {min} e {max} caracteres!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private String descricao;
	
	@Min(value=0, message="O valor n�o pode ser menor que zero!")
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Double percentual;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@Column(nullable = false)
	private Boolean status;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne
	private Fornecedor fornecedor;
	
	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Desconto other = (Desconto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
