package br.leonardo.sisconp.entity.pessoa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CNPJ;

@Entity

public class Juridico extends ClienteFinal {

	@NotNull( message="O valor n�o pode ser nulo!")
	@CNPJ(message="O campo deve conter um CNPJ valido!")
	@Column(length = 18, nullable = false)
	private String doc;

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	

	
	
}
