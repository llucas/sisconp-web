package br.leonardo.sisconp.entity.pessoa;


import java.util.List;

import javax.persistence.Entity;

import br.leonardo.sisconp.entity.pedido.Pedido;
import br.leonardo.sisconp.entity.produto.Item;
import br.leonardo.sisconp.entity.pedido.Operacao;
import javax.persistence.Column;
import javax.persistence.OneToMany;

@Entity
public class Fornecedor extends Juridica {

	
	@OneToMany(mappedBy = "fornecedor")
	private List<Representante> representantes;

	@OneToMany(mappedBy = "fornecedor")
	private List<Pedido> pedidos;

	@OneToMany(mappedBy = "fornecedor")
	private List<Item> itens;

	@OneToMany(mappedBy = "fornecedor")
	private List<Operacao> operacoes;

	

	public List<Representante> getRepresentantes() {
		return representantes;
	}

	public void setRepresentantes(List<Representante> representantes) {
		this.representantes = representantes;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public List<Operacao> getOperacoes() {
		return operacoes;
	}

	public void setOperacoes(List<Operacao> operacoes) {
		this.operacoes = operacoes;
	}

	public Fornecedor(String endereco, String nome, String inscricao, String numero, Boolean status,
			String email, String telefone, String bairro, String cep, String cidade, String estado, String ibge,
			String observacao, Usuario usuario, String cnpj, String logo, List<Usuario> comuns, Usuario administrador,
			List<Representante> representantes, List<Pedido> pedidos, List<Item> itens, List<Operacao> operacoes) {
		super(endereco, nome, inscricao, numero, status, email, telefone, bairro, cep, cidade, estado, ibge,
				observacao, usuario, cnpj, logo, comuns, administrador);
		this.representantes = representantes;
		this.pedidos = pedidos;
		this.itens = itens;
		this.operacoes = operacoes;
	}

	public Fornecedor() {
		super();
	}
	
	
	
}
