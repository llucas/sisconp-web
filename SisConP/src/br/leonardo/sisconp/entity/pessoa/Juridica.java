package br.leonardo.sisconp.entity.pessoa;


import static javax.persistence.InheritanceType.JOINED;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CNPJ;
import static javax.persistence.CascadeType.ALL;

@Entity
@Inheritance(strategy = JOINED)
public abstract class Juridica extends Pessoa {

	@NotNull( message="O valor n�o pode ser nulo!")
	@CNPJ(message="O campo deve conter um CNPJ valido!")
	@Column(length = 18, nullable = false)
	private String cnpj;

	@Column(nullable = true)
	private String logo;

	@OneToMany(mappedBy = "juridica")
	private List<Usuario> comuns;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false, cascade = ALL)
	private Usuario administrador;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<Usuario> getComuns() {
		return comuns;
	}

	public void setComuns(List<Usuario> comuns) {
		this.comuns = comuns;
	}

	public Usuario getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Usuario administrador) {
		this.administrador = administrador;
	}

	public Juridica() {
		super();
	}

	public Juridica( String endereco, String nome, String inscricao, String numero, Boolean status,
			String email, String telefone, String bairro, String cep, String cidade, String estado, String ibge,
			String observacao, Usuario usuario, String cnpj, String logo, List<Usuario> comuns, Usuario administrador) {
		super( endereco, nome, inscricao, numero, status, email, telefone, bairro, cep, cidade, estado, ibge,
				observacao, usuario);
		this.cnpj = cnpj;
		this.logo = logo;
		this.comuns = comuns;
		this.administrador = administrador;
	}

	
	
	
	
}
