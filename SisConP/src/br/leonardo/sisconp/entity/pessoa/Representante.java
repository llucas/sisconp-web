package br.leonardo.sisconp.entity.pessoa;


import java.util.List;

import javax.persistence.Entity;

import br.leonardo.sisconp.entity.pedido.Pedido;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
public class Representante extends Juridica {
	
	@Length(max = 150, message = "O campo deve ter no m�ximo {max} caracteres!")
	@Column(nullable = true, length = 150)
	private String regiao;

	@OneToMany(mappedBy = "representante")
	private List<Pedido> pedidos;

	@OneToMany(mappedBy = "representante")
	private List<Revenda> revendas;

	@NotNull( message="O valor n�o pode ser nulo!")
	@ManyToOne(optional = false)
	private Fornecedor fornecedor;

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}


	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public List<Revenda> getRevendas() {
		return revendas;
	}

	public void setRevendas(List<Revenda> revendas) {
		this.revendas = revendas;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	
}
