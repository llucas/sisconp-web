package br.leonardo.sisconp.testes;

import org.junit.Assert;
import org.junit.Test;

import br.leonardo.sisconp.conexao.DAO;
import br.leonardo.sisconp.entity.pessoa.Fornecedor;
import br.leonardo.sisconp.entity.pessoa.Usuario;
import br.leonardo.sisconp.web.controller.AppController;
import br.leonardo.sisconp.web.controller.pessoa.PessoaController;
import br.leonardo.sisconp.web.controller.usuario.CadastroController;

public class Testes {

	@Test
	public void teste01() {

		DAO<Usuario> dao = new DAO<Usuario>(Usuario.class);
		int lista1 = dao.getInstanceList().size();

		CadastroController cadastro = new CadastroController();
		cadastro.initNew();

		cadastro.setUsuario(new Usuario(null, "012345", "jaque", "Jacqueline K. Lund", null,
				"JacquelineKLund@teleworm.us", true, null, null));
		cadastro.cadastra();
		int lista2 = dao.getInstanceList().size();

		Assert.assertEquals(lista1, lista2 - 1);

	}

	@Test
	public void teste02() {
		DAO<Usuario> dao = new DAO<Usuario>(Usuario.class);
		int lista1 = dao.getInstanceList().size();

		CadastroController cadastro = new CadastroController();
		cadastro.initNew();

		cadastro.setUsuario(new Usuario(null, null, null, null, null, null, true, null, null));
		try {
			cadastro.cadastra();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int lista2 = dao.getInstanceList().size();

		Assert.assertEquals(lista1, lista2);

	}

	@Test
	public void teste03() {
		DAO<Usuario> dao = new DAO<Usuario>(Usuario.class);
		int lista1 = dao.getInstanceList().size();

		CadastroController cadastro = new CadastroController();
		cadastro.initNew();
		try {
			// id
			// senha
			cadastro.setUsuario(new Usuario(null, null, "jaque1", "Jacqueline K. Lund", null,
					"JacquelineKLund@teleworm.us", true, null, null));
			cadastro.cadastra();
		} catch (Exception e) {

		}

		try {
			// login
			cadastro.setUsuario(new Usuario(null, "012345", null, "Jacqueline K. Lund", null,
					"JacquelineKLund@teleworm.us", true, null, null));
			cadastro.cadastra();

		} catch (Exception e) {

		}

		try {
			// nome
			cadastro.setUsuario(
					new Usuario(null, "012345", "jaque2", null, null, "JacquelineKLund@teleworm.us", true, null, null));
			cadastro.cadastra();

		} catch (Exception e) {

		}

		try {
			// email
			cadastro.setUsuario(
					new Usuario(null, "012345", "jaque4", "Jacqueline K. Lund", null, null, true, null, null));
			cadastro.cadastra();
		} catch (Exception e) {

		}

		int lista2 = dao.getInstanceList().size();

		Assert.assertEquals(lista1, lista2);

	}

	@Test
	public void teste04() {
		DAO<Usuario> dao = new DAO<Usuario>(Usuario.class);
	
		CadastroController cadastro = new CadastroController();
		cadastro.initNew();

		cadastro.setUsuario(new Usuario(null, "012345", "jaque", "Jacqueline K. Lund", null,
				"JacquelineKLund@teleworm.us", true, null, null));
		try {
			cadastro.cadastra();

		cadastro.getUsuario().setJuridica(new Fornecedor("Rua Paulo Freire", "Paulo e Bianca Marcenaria Ltda", "384/5787963",
							"440", true, "qualidade@paulobianca.com.br", "(51) 2652-6219", "Tr�s Marias", "99470000",
							"Esteio", "RS", null, null, cadastro.getUsuario(), "31.229.033/0001-31", null, null,
							cadastro.getUsuario(), null, null, null, null));
			
		dao.merge(cadastro.getUsuario());
		} catch (Exception e) {
			e.printStackTrace();
		}
	

		Assert.assertEquals(cadastro.getUsuario().getJuridica().getCep(), "99470000");

	}
	
	@Test
	public void teste05() {
		DAO<Usuario> dao = new DAO<Usuario>(Usuario.class);
		dao.merge(new Usuario(null, "ohg3saeK2".hashCode()+"", "Pird1982", "Irene J. Barber", null, "IreneJBarber@jourrapide.com", true, null, null));
		AppController app= new AppController();
		app.init();
		app.setLogin("Pird1982");
		app.setSenha("ohg3saeK2");
		Assert.assertEquals("private/empresa?faces-redirect=true",app.doLogin());

	}

}
