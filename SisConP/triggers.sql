CREATE OR REPLACE FUNCTION funcao_geral_log() RETURNS TRIGGER AS $body$
DECLARE
    v_old_data TEXT;
    v_new_data TEXT;
BEGIN
    /*  If this actually for real auditing (where you need to log EVERY action),
        then you would need to use something like dblink or plperl that could log outside the transaction,
        regardless of whether the transaction committed or rolled back.
    */
 
    /* This dance with casting the NEW and OLD values to a ROW is not necessary in pg 9.0+ */
 
    IF (TG_OP = 'UPDATE') THEN
        v_old_data := ROW(OLD.*);
        v_new_data := ROW(NEW.*);
        INSERT INTO log.log (id,tabela,acao,original,novo,query) 
        VALUES (nextval('seq_log'),TG_TABLE_NAME::TEXT,substring(TG_OP,1,1),v_old_data,v_new_data, current_query());
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        v_old_data := ROW(OLD.*);
        INSERT INTO log.log (id,tabela,acao,original,query)
        VALUES (nextval('seq_log'),TG_TABLE_NAME::TEXT,substring(TG_OP,1,1),v_old_data, current_query());
        RETURN OLD;
    ELSIF (TG_OP = 'INSERT') THEN
        v_new_data := ROW(NEW.*);
        INSERT INTO log.log (id,tabela,acao,novo,query)
        VALUES (nextval('seq_log'),TG_TABLE_NAME::TEXT,substring(TG_OP,1,1),v_new_data, current_query());
        RETURN NEW;
    END IF;
	
END;
$body$
LANGUAGE plpgsql;

--  https://wiki.postgresql.org/wiki/Audit_trigger
--nao executar(gera os create trigger)
select 'create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON ' ||tablename||
	' FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();'
	from pg_tables where schemaname='public'
--fim nao executar

create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON baixa FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON parcela FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON juridico FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON item FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON juridica FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON operacao FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON pedidofinal FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON fisica FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON administrador FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON pedido FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON anexo FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON fornecedor FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON revenda FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON usuario FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON pessoa FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON comum FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON desconto FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON itempedido FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON clientefinal FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();
create trigger tr_logs_gerais AFTER INSERT OR UPDATE OR DELETE ON representante FOR EACH ROW EXECUTE PROCEDURE funcao_geral_log();


